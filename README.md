# graneet-technical-assesment

## Prerequisites

- Docker (with `docker compose`)
- Node v14 + NPM
- Yarn

## Installation 

> sh start.sh

This command will : 
- Build and start a docker image with Postgres
- Install both client and server NPM packages
- Start both client and server watching files updates
- Open the client on the browser


## Ports used

- **Client** : http://localhost:6500
- **Server** : http://localhost:6501