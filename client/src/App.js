import { useEffect, useRef, useState } from 'react';
import { Button, FormControl, FormLabel, Heading, HStack, IconButton, Input, Spinner, Text, VStack } from '@chakra-ui/react';
import { AddIcon, DeleteIcon } from '@chakra-ui/icons';

function CartItem({ item, reloadItems }) {
  const [isPriceUpdating, setIsPriceUpdating] = useState(false);
  const [isQuantityUpdating, setIsQuantityUpdating] = useState(false);

  const priceRef = useRef();
  const quantityRef = useRef(); 


  const getPricePromise = () => {
    const price = priceRef.current.value;
    setIsPriceUpdating(true);
    return fetch('http://localhost:6501/' + item.id + '/price/' + price).then(() => {
      setIsPriceUpdating(false);
    });
  };

  const getQuantityPromise = () => {
    const quantity = quantityRef.current.value;
    setIsQuantityUpdating(true);
    return fetch('http://localhost:6501/' + item.id + '/quantity/' + quantity).then(() => {
      setIsQuantityUpdating(false);
    });
  };

  const updateAttributes = ({ price, quantity }) => () => {
    const promises = [];
    if (price) promises.push(getPricePromise());
    if (quantity) promises.push(getQuantityPromise());

    Promise.all(promises).then(() => {
      reloadItems();
    });
  };

  const deleteItem = () => (
    fetch('http://localhost:6501/' + item.id + '/delete').then(() => {
      reloadItems();
    })
  );

  useEffect(() => {
    priceRef.current.value = item.price;
    quantityRef.current.value = item.quantity;
  }, [item]);

  return (
    <VStack gap={6} bg="gray.50" p={6}>
      <Heading size="md">
        <HStack gap={6}>
          <Text>{item.productName}</Text>
          <IconButton
            onClick={deleteItem}
            icon={<DeleteIcon />}
            colorScheme="red"
            variant="outline"
          />
        </HStack>
      </Heading>
      <HStack gap={2}>
        <VStack gap={2}>
          <FormControl>
            <FormLabel>Price</FormLabel>
            <Input type="number" ref={priceRef} />
          </FormControl>

          <Button 
            onClick={updateAttributes({ price: true })}
            isLoading={isPriceUpdating}
          >
            Update price
          </Button>
        </VStack>

        <VStack gap={2}>
          <FormControl>
            <FormLabel>Quantity</FormLabel>
            <Input type="number" ref={quantityRef} />
          </FormControl>

          <Button
            onClick={updateAttributes({ quantity : true })}
            isLoading={isQuantityUpdating}
          >
            Update quantity
          </Button>
        </VStack>

        <VStack gap={2}>
          <FormControl>
            <FormLabel>Total</FormLabel>
            <Text h={10} p={3}>
              {(isQuantityUpdating || isPriceUpdating) ? <Spinner /> : item.total}
            </Text>
          </FormControl>
          
          <Button onClick={updateAttributes({ price: true, quantity: true })}>Update both</Button>
        </VStack>
      </HStack>
    </VStack>
  );
}

function App() {
  const [data, setData] = useState({ list: [], total: 0 });
  const [isLoading, setIsLoading] = useState(false);
  const loadItems = () => {
    setIsLoading(true);
    fetch('http://localhost:6501/data').then((response) => {
      response.json().then((data) => {
        setData(data);
        setIsLoading(false);
      });
    });
  };

  const addItem = () => (
    fetch('http://localhost:6501/add').then(() => {
      loadItems();
    })
  );

  useEffect(() => {
    loadItems();
  }, [])
  

  return (
    <VStack gap={6} m={6}>
      {data.list.map((item) => (
        <CartItem item={item} reloadItems={loadItems} />
      ))}

      <HStack gap={6}>
        <Text>TOTAL : {isLoading ? <Spinner /> : data.total}</Text>
        <Button
          onClick={addItem}
          rightIcon={<AddIcon />}
          colorScheme="blue"
        >
          Add item
        </Button>
      </HStack>
    </VStack>
  )
}

export default App;
