import { Controller, Get, Param, OnModuleInit } from '@nestjs/common';
import { FindManyOptions } from 'typeorm';

import { CartRepository, ItemRepository } from './app.repository';
import { Item } from './entities/item.entity';
import { StatusResponse } from './types/status-response';

import info from '../package.json';

@Controller()
export class AppController implements OnModuleInit {
  constructor(
    private readonly itemRepo: ItemRepository,
    private readonly cartRepo: CartRepository,
  ) {}

  // Method used in order to populate the database at the beginning
  // It is also called after each update of the codebase (watch)
  async onModuleInit() {
    const items: Item[] = [
      {
        id: 1,
        price: 100,
        quantity: 1,
        total: 100,
      },
      {
        id: 2,
        price: 50,
        quantity: 3,
        total: 150,
      },
    ];

    let total = 0;
    for (let i = 0; i < items.length; i++) {
      total += items[i].total;
    }

    await this.itemRepo.save(items[0]);
    await this.itemRepo.save(items[1]);
    await this.cartRepo.save({ total });
  }

  @Get()
  status(): StatusResponse {
    const { name, version } = info;

    return {
      name,
      version,
      status: 'good',
    };
  }

  @Get('data')
  async getData() {
    const cart = await this.cartRepo.findOne();

    const options: FindManyOptions<Item> = { order: { id: 'ASC' } };
    return this.itemRepo.find(options).then((items) => {
      const itemsFormatted = [];
      for (const item of items) {
        itemsFormatted.push({
          ...item,
          // productName: 'Product ' + item.id,
        });
      }
      return {
        list: itemsFormatted,
        total: cart.total,
      };
    });
  }

  @Get(':id/price/:price')
  async updatePrice(@Param() params) {
    const { id, price } = params;
    const cart = await this.cartRepo.findOne();
    const item = await this.itemRepo.findOneAndWait(id);

    const total = item.quantity * price;
    const delta = total - item.total;
    await this.cartRepo.save({ ...cart, total: cart.total + delta });
    return this.itemRepo.save({ ...item, price, total });
  }

  @Get(':id/quantity/:quantity')
  async updateQuantity(@Param() params) {
    const { id, quantity } = params;
    const cart = await this.cartRepo.findOne();
    const item = await this.itemRepo.findOneAndWait(id);

    const total = quantity * item.price;
    const delta = total - item.total;
    await this.cartRepo.save({ ...cart, total: cart.total + delta });
    return this.itemRepo.save({ ...item, quantity, total });
  }

  @Get(':id/delete')
  async deleteItem(@Param() params) {
    const { id } = params;
    const item = await this.itemRepo.findOne(id);
    const cart = await this.cartRepo.findOne();
    await this.cartRepo.save({ ...cart, total: cart.total - item.total });
    return this.itemRepo.remove(item);
  }

  @Get('add')
  async addItem() {
    const count = await this.itemRepo.count();
    const cart = await this.cartRepo.findOne();
    await this.cartRepo.save({ ...cart, total: cart.total + 100 });
    return this.itemRepo.save({
      id: count + 1,
      price: 100,
      quantity: 1,
      total: 100,
    });
  }
}
