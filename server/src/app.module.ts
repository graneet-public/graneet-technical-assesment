import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { CartRepository, ItemRepository } from './app.repository';
import { getTypeOrmConfig } from './config/utils';
import { Cart } from './entities/cart.entity';
import { Item } from './entities/item.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(getTypeOrmConfig()),
    TypeOrmModule.forFeature([Cart, Item, CartRepository, ItemRepository]),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
