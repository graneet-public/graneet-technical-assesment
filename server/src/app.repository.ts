import { EntityRepository, Repository } from 'typeorm';
import { Cart } from './entities/cart.entity';
import { Item } from './entities/item.entity';

const sleep = (duration: number): Promise<void> =>
  new Promise((resolve) => {
    setTimeout(resolve, duration);
  });

// Extended repository use to simulate action duration
class ExtendedRepository<Entity> extends Repository<Entity> {
  async findOneAndWait(id?: number | string): Promise<Entity> {
    const entity = await this.findOneOrFail(id);
    await sleep(1000);
    return entity;
  }
}

@EntityRepository(Item)
export class ItemRepository extends ExtendedRepository<Item> {}

@EntityRepository(Cart)
export class CartRepository extends ExtendedRepository<Cart> {}
