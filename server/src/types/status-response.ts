export interface StatusResponse {
  name: string;
  version: string;
  status: string;
}
