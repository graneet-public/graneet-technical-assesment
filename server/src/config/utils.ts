import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import {
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_DB_NAME,
} from './constants';

export const getTypeOrmConfig = (): PostgresConnectionOptions => ({
  type: 'postgres',
  host: DATABASE_HOST as string,
  port: DATABASE_PORT as number,
  username: DATABASE_USERNAME as string,
  password: DATABASE_PASSWORD as string,
  database: DATABASE_DB_NAME as string,
  entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  synchronize: true,
  dropSchema: true,
  logging: false,
});
