const getEnv = (
  key: string,
  defaultValue?: string | number | boolean,
  forceType?: 'boolean' | 'number',
): string | boolean | number => {
  const envValue = process.env[key];

  const value = envValue ?? defaultValue;

  if (value === undefined) {
    throw new Error(`missing-env-value: [${key}]`);
  }

  if (!forceType) {
    return value;
  }

  if (forceType === 'boolean') {
    if (typeof value === 'boolean') {
      return value;
    }
    return value === 'true';
  }

  if (forceType === 'number') {
    if (typeof value === 'number') {
      return value;
    }
    if (typeof value === 'boolean') {
      throw new Error(`invalid-type-for: [${key}] boolean <> number`);
    }
    return parseInt(value, 10);
  }
};

export const DATABASE_HOST = getEnv('DATABASE_HOST');

export const DATABASE_USERNAME = getEnv('DATABASE_USERNAME');

export const DATABASE_PASSWORD = getEnv('DATABASE_PASSWORD');

export const DATABASE_DB_NAME = getEnv('DATABASE_DB_NAME');

export const DATABASE_PORT = getEnv('DATABASE_PORT', undefined, 'number');
