#!/bin/sh

# 1. Execute Docker Compose (database)
# 2. Install npm packages
# 2. Start client (watch) -> PORT = 6500
# 3. Start server (watch) -> PORT = 6501

docker-compose up -d && \
yarn --cwd client install &&
yarn --cwd server install && \
  (yarn --cwd server start:dev & \
  (sleep 1 && export PORT=6500 && yarn --cwd client start) & \
  wait)
